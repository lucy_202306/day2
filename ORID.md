# Day02-ORID

## O

​	In the morning, I learned the method of function naming, the role of Tasking in development and how to task a question. I also learned the common syntax of Git and six commit types, namely feat, fix, refactor, test, docs and chore.
​	In the afternoon, I learned Context Map and how to draw a Context Map. Tasking and Context Map construction were performed in two classroom exercises: Multiplication Table and Receipt By Barcodes to deepen the consolidation of the learned knowledge. I have learned java by the code implementation of Multiplication Table.

## R

I was worried

## I

Why learn to draw Context Maps? Splitting the problem can reduce the coupling degree of function as much as possible in the development process

## D

I am not proficient in the use of Context Map and not familiar with java, so I need to learn more.